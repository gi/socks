# Software for colored _k_-mer sets · Interface & Benchmark

The aim of this project is to evaluate different software tools for computational pangenomics and to establish a standard interface for sets of _k_-mer sets index structures. The first virtual meeting with the authors and other interested people from the community was held on March 23, 2021 ([slides](https://gitlab.ub.uni-bielefeld.de/gi/socks/-/blob/main/slides/2021-03-23.pdf)).

‎  
## Core interface (as decided on March 23, 2021)

- **`build`**: construct an index from a set of sequences

  - **input**: plain text file containing the sequence file names, e.g.:  
‎  
    ```
    COLOR_NAME_1: /PATH/TO/GENOME.FASTA
    COLOR_NAME_2: /PATH/TO/READ_1.FASTQ /PATH/TO/READ_2.FASTQ
    ```
  - **output**: index in binary or interoperable format, e.g. [kmer file format](https://github.com/Kmer-File-Format/kff-reference)  
    ‎ ‎ ‎ ‎ ‎ _(at least one of the options, binary or interoperable format, should be provided)_  
    ‎ ‎ ‎ ‎ ‎ _(if both options are provided, it should be possible to switch using a parameter)_  

---

- **`lookup-kmer`**: find the color sets for a list of _k_-mers

  - **input**: plain text file containing the _k_-mers, one per line, e.g.:  
‎  
    ```
    ACGTACGT
    ACCTAGGT
    ```
  - **output**: plain text file listing the color set for each _k_-mer, e.g.:  
‎  
    (as list of positive hits)
    ```
    ACGTACGT: COLOR_NAME_1 COLOR_NAME_4 COLOR_NAME_7 ...
    ACCTAGGT: COLOR_NAME_1 COLOR_NAME_5 COLOR_NAME_8 ...
    ```
    (or as a binary vector)
    ```
    ACGTACGT: 10010010...
    ACCTAGGT: 10001001...
    ```
    _(at least one of the options, positive hits or binary vector, should be provided)_  
    _(if both options are provided, it should be possible to switch using a parameter)_  

---

- **`lookup-color`**: find the _k_-mer sets for a list of colors

  - **input**: plain text file containing color names, one per line, e.g.:  
‎  
    ```
    COLOR_NAME_1
    COLOR_NAME_2
    ```
  - **output**: plain text file listing the _k_-mer set for each color, e.g.:  
‎  
    (as list of positive hits)
    ```
    COLOR_NAME_1: ACCTAGGT ACGTACGT CAAGCGTA ...
    COLOR_NAME_2: AGCTAGCT AGGTACCT CAGGCATA ...
    ```
‎  
## List of software tools

- **SeqOthello**: ultra-fast and memory-efficient indexing structure for RNA-seq experiments  
  https://github.com/LiuBioinfo/SeqOthello

- **Mantis**: fast, small & exact large-scale sequence-search index for raw-read experiments  
  https://github.com/splatlab/mantis

- **SBT/SSBT**: bloom-tree based index and search for short-read sequencing experiments  
  https://github.com/Kingsford-Group/bloomtree  
  https://github.com/Kingsford-Group/splitsbt

- **AllSome-/HowDeSBT**: improved index and search for short-read sequencing experiments  
  https://github.com/medvedevgroup/bloomtree-allsome  
  https://github.com/medvedevgroup/HowDeSBT

- **Cortex**: efficient & low-memory software for consensus assembly and variation analysis  
  https://github.com/iqbal-lab/cortex

- **McCortex**: population de novo assembly and variant calling using linked de Bruijn graphs  
  https://github.com/mcveanlab/mccortex

- **BIGSI**: bit-sliced signature index for ultra-fast search in bacterial and viral genomic data  
  https://github.com/iqbal-lab-org/bigsi

- **COBS**: compact bit-sliced signature index for ultra-fast search in large-scale genomic data  
  https://github.com/bingmann/cobs

- **RAMBO**: bloom filter for ultra-fast multiple set membership testing on large-scale data  
  https://github.com/RUSH-LAB/RAMBO

- **Cosmo/VARI**: fast construction and merging of succinct colored de Bruijn graphs  
  https://github.com/cosmo-team/cosmo/tree/VARI

- **Rainbowfish**: succinct data structure for storing and querying colored de Bruijn graphs  
  https://github.com/COMBINE-lab/rainbowfish

- **BFT**: alignment- and reference-free succinct data structure for colored de Bruijn graphs  
  https://github.com/GuillaumeHolley/BloomFilterTrie

- **Bifrost**: parallel construction, indexing & querying of colored compacted de Bruijn graphs  
  https://github.com/pmelsted/bifrost

- **kmtricks**: _k_-mer counting matrix and bloom filter construction from sets of raw read sets  
  https://github.com/tlemane/kmtricks

- **kcollections**: fast and memory-efficient data structure library for storing _k_-mer collections  
  https://github.com/masakistan/kcollections

- **Raptor**: fast & space-efficient filter for querying large collections of nucleotide sequences  
  https://github.com/seqan/raptor

- **Themisto**: space-efficient pseudo-alignment using compressed colored de Bruijn graphs  
  https://github.com/algbio/Themisto

- **REINDEER**: efficient indexing of _k_-mer presence and abundance in sequencing datasets  
  https://github.com/kamimrcht/REINDEER

- **GATB-core**: highly efficient algorithms for next generation sequencing data analysis  
  https://github.com/GATB/gatb-core
